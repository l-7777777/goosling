use crate::arch::native::ErrorCode;
use crate::arch::{CpuInfo, Error};
use limine::LimineSmpInfo;

pub trait PagingManagerTrait {
    type PageTable: PageTableTrait;
    fn set_page_table(page_table: &Self::PageTable) -> Result<(), Error>;
    fn get_page_table<'a>() -> Result<&'a mut Self::PageTable, Error>;
}

pub trait PageTableTrait {
    fn map_page(&mut self, virtual_addr: usize, physical_addr: usize);
    fn unmap_page(&mut self, virtual_addr: usize);
    fn get_physical_addr(&self, virtual_addr: usize) -> Option<usize>;
}

pub trait UtilTrait {
    fn init() -> Result<(), Error>;
    fn halt_loop() -> !;
}

pub trait InterruptInfoTrait {
    fn interrupt_num(&self) -> usize;
    fn error_code(&self) -> usize;
}

pub trait InterruptTableTrait {
    type CpuState: CpuStateTrait;
    fn set_interrupt_handler(
        &mut self,
        interrupt_num: usize,
        handler: fn(Option<ErrorCode>, u64, &mut Self::CpuState),
    );
    fn new() -> Self;
}

pub trait InterruptManagerTrait {
    type InterruptTable: InterruptTableTrait;
    fn set_interrupt_table(interrupt_table: &mut Self::InterruptTable) -> Result<(), Error>;
    fn get_interrupt_table<'a>() -> Result<&'a mut Self::InterruptTable, Error>;
    fn enable_interrupts();
}

pub trait CpuTrait {
    type PageTable: PageTableTrait;
    type InterruptTable: InterruptTableTrait;
    fn init_cpu(limine_jump_addr: &mut fn(LimineSmpInfo), callback: fn(Self)) -> Result<(), Error>;
    fn set_page_table(page_table: &Self::PageTable) -> Result<(), Error>;
    fn get_page_table<'a>() -> Result<&'a mut Self::PageTable, Error>;
    fn set_interrupt_table(interrupt_table: &mut Self::InterruptTable) -> Result<(), Error>;
    fn get_interrupt_table<'a>() -> Result<&'a mut Self::InterruptTable, Error>;
    fn get_info() -> Result<CpuInfo, Error>;
}

pub trait CpuStateTrait {
    fn get_ip(&self) -> usize;
    fn set_ip(&mut self, ip: usize);
}
